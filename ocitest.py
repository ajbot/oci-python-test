import oci
# fill in the blanks with these values
bucketname = "PythonTestUploadBucket"
namespace = ""
profile = "DEFAULT"
configPath = ".oci/config"

# give a name to the file as it will appear in the bucket
destfilename="testfile123.txt"

# define the full path to your source file
sourcefile="testfile123.txt"

# define your OCI config
config = oci.config.from_file(configPath,profile)

#### create an ObjectStorage cLient using our config above ####
storage = oci.object_storage.ObjectStorageClient(config)

# we'll create and write this file, for testing purposes. Obviously you don't need this if you're using an already existing file
with open(sourcefile, "a") as file:
    file.seek(0)
    file.truncate()
    file.write("This is a test file")

try:
    print("putting file...")
    storage.put_object(namespace,bucketname,destfilename,sourcefile)
except Exception as e:
    print("putting failed: %s" % e)
    exit(1)

print("file put. Retrieving all objects")
bucketObjects = storage.list_objects(namespace,bucketname).data
for o in bucketObjects.objects:
    print("File found in bucket: %s" % o.name)

exit(0)